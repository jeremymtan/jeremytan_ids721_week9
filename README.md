[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_week9/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan_ids721_week9/-/commits/main)
# JeremyTan_IDS721_Week9
The project is a Streamlit web application integrated with the Hugging Face model for Visual Question Answering (VQA). It utilizes Docker for containerization and showcases image processing capabilities. I planned to deploy the image via a cluster, but the task runner kept running but no container showed up. 

## EC2 Hosted Streamlit Functioning Web App (Extra Credit)
http://54.81.100.189:8501/

![Screenshot_2024-04-09_at_5.30.05_AM](/uploads/1610331a8ecc13d7895537e9986f91b9/Screenshot_2024-04-09_at_5.30.05_AM.png)


## Connection to Open Source LLM (Hugging Face Model Integration):
- **API Interaction:** The application interfaces with the Hugging Face VQA model (`query` function) via API calls. It sends images along with a predefined question to get predictions. I use the Hugging Face Inference API 

## Aesthetics/Creativity of Site:
- **User Interaction:** Users can choose to upload an image for prediction or use a default image. The application processes the uploaded image or the default image using the integrated model. It tells them if the indvidual is doing the act of studying or if the individual is presently studying. If not, it tells them they're not studying. 

## Chatbot Performance: 
- The VQA model seems to be able to distinguish between if an individual is studying or not studying accurately the majority of the time. However, the image has to have a strong signal of "studying" (i.e. shows them paying attention to a whiteboard) 

## Preparation: 
1. Install streamlit
2. Decide on your model to connect to, you can browse various ones on Hugging Face 
3. Create an `.env` file and place your api token in the file 
4. Design your streamlit app (i.e. Do you want buttons? Title? A picture?)
5. Once you make the app do `streamlit run main.py` to test it locally it should go to `http://localhost:8501`
6. If you're satisified with your app, you can push your repo (needed for next step)
7. Sign into AWS to create your EC2 isntance 
8. Launch Instance -> use Ubuntu as base image -> keep t2.micro -> allow HTTP and allow HTTPS -> edit -> add security group rule -> choose "CUSTOM TCP", port 8501, and choose source type "ANYWHERE"
9. Finish launching wait for instance to provision fully 
10. Click connect to instance, use the provided aws window 
11. Set up you environment: 
12. git clone your repo
13. sudo apt update
14. sudo apt-get install -y python3-pip
15. cd to your repo
16. pip3 install -r requirements.txt
17. export PATH=~/.local/bin/:$PATH
18. pip install --upgrade jinja2 
19. add your .env file -> touch .env -> vi .env -> copy your local .env and paste it in -> then type ":wq"
20. test your app by doing `streamlit run main.py`
21. After you confirm its exposed, set up a screen so you can close your session without terminating the app: type `screen`, press spacebar, then do `streamlit run main.py`
22. run your streamlit app and then detach the screen `ctrl` then `a` then `d`

## References 
First few I attempted to make a cluster and then deploy and image via task but was unsuccessful:
1. https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecr-repositories.html
2. https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-iam-roles.html
3. https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecr-repositories.html
4. https://repost.aws/knowledge-center/ecs-container-instance-requirement-error

So I ended it up just self hosting the site on the instance:

5. https://adzic-tanja.medium.com/deploying-a-streamlit-app-using-docker-aws-ecr-and-ec2-ad6c15a0b225
6. https://github.com/upendrak/streamlit-aws-tutorial?tab=readme-ov-file
7. https://stackoverflow.com/questions/45181813/how-to-configure-git-in-ec2-with-my-local-machine
8. https://medium.com/@otrinh/deploying-streamlit-app-to-ec2-instance-7a7edeffbb54
9. https://discuss.streamlit.io/t/hosting-streamlit-with-aws-ec2/234
10. https://discuss.streamlit.io/t/how-to-clear-and-replace-an-image/26618
11. https://discuss.streamlit.io/t/clear-the-text-in-text-input/2225
12. https://docs.streamlit.io/develop/tutorials/llms/llm-quickstart
13. https://docs.streamlit.io/develop/api-reference/caching-and-state