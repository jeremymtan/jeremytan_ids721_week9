import streamlit as st
from dotenv import load_dotenv
import os
import requests
import base64
import json

API_TOKEN = os.getenv("API_TOKEN")

HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}
API_URL = "https://api-inference.huggingface.co/models/google/gemma-7b"


def query(filename, uploaded=True):
    """query hugging face inferene api"""
    data = None
    if uploaded is False:
        with open(filename, "rb") as f:
            data = f.read()
    else:
        data = filename

    encoded_data = base64.b64encode(data).decode("utf-8")

    payload = {
        "inputs": {
            "question": "is this person studying?",
            "image": encoded_data,
        }
    }

    response = requests.post(API_URL, headers=HEADERS, json=payload)
    return json.loads(response.content.decode("utf-8"))


def main():
    """Main entry point"""
    st.title("Study or Not Study?")
    image = st.empty()
    image.image(
        "static/final.jpg", caption="Individual NOT Studying in Noah Gift's Class"
    )
    placeholder = st.empty()
    uploaded_file = st.file_uploader("Choose a file")
    if st.button("Use Default Image"):
        image.image(
            "static/final.jpg", caption="Individual NOT Studying in Noah Gift's Class"
        )
        result = query("static/final.jpg", uploaded=False)
        yes_answer_score = [item["score"] for item in result if item["answer"] == "yes"]
        no_answer_score = [item["score"] for item in result if item["answer"] == "no"]
        yes_answer_score = yes_answer_score[0] if yes_answer_score else 0
        no_answer_score = no_answer_score[0] if no_answer_score else 0
        # grab results and check
        if no_answer_score > yes_answer_score:
            placeholder.text("Not Study!")
            uploaded_file = None
        else:
            placeholder.text("Study!")
            uploaded_file = None

    if uploaded_file is not None:
        if uploaded_file.type.startswith("image"):
            image.image(uploaded_file, caption="User Uploaded Image")
            # may timeout but should be fine
            result = query(uploaded_file.getvalue())
            yes_answer_score = [
                item["score"] for item in result if item["answer"] == "yes"
            ]
            no_answer_score = [
                item["score"] for item in result if item["answer"] == "no"
            ]
            yes_answer_score = yes_answer_score[0] if yes_answer_score else 0
            no_answer_score = no_answer_score[0] if no_answer_score else 0
            # grab results and check
            if no_answer_score > yes_answer_score:
                placeholder.text("Not Study!")
            else:
                placeholder.text("Study!")
        else:
            placeholder.text("The uploaded file is not an image.")


if __name__ == "__main__":
    load_dotenv()
    main()
