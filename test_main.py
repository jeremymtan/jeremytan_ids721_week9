"""
Test goes here

"""

import os
from dotenv import load_dotenv
import requests


load_dotenv()
API_TOKEN = os.getenv("API_TOKEN")

HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}
API_URL = "https://api-inference.huggingface.co/models/google/gemma-7b"


def test_static_files(directory="static/"):
    """checks static files exists"""
    s_files = [f for f in os.listdir(directory) if f.endswith(".jpg")]

    for s_file in s_files:
        file_path = os.path.join(directory, s_file)
        assert os.path.exists(file_path) and os.path.isfile(file_path)


def test_api():
    """checks hugging api token works"""
    test_payload = {"inputs": "The answer to the universe is"}
    response = requests.post(API_URL, headers=HEADERS, json=test_payload)
    assert response.status_code == 200


if __name__ == "__main__":
    test_static_files()
    test_api()
